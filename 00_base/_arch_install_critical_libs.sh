#!/bin/bash

# Localize <scope & context> important <parent> Script Variables here ...
_tagScriptID="$TAG_ID_SCRIPT"
_actSrcDir="$ACT_DIR_ARCH_SETUP_SCRIPT_ErichM"

#? INFO: Install the CRITICAL <required> Arch Linux 3rd Party Libraries <software packages> !!!

# STEP 1 - Install the best AUR Helper -> 'yay' !!!
echo " -> $_tagScriptID: STEP 01 - Install the AUR Helper -> 'yay' < CRITICAL > !!!"
_pkgYAY=yay
if pacman -Qi $_pkgYAY > /dev/null ; then
  echo " -> $_tagScriptID: The package '$_pkgYAY' is already installed !!!"
else
  # 2.1 - Install the basic <required> 3rd Party Packages ...
  echo -en '\n' && sleep 1
  echo " -> $_tagScriptID: Installing 'base-devel', 'git' & 'wget' ..."
  sudo -E pacman -S --noconfirm --needed base-devel git wget
  #! unset sudo   #! <- IMPORTANT !!! ... <- NOT NEEDED ANYMORE !!!

  # 2.2 - Clone the 'yay' GitHub Repository ...
  echo -en '\n' && sleep 1
  echo " -> $_tagScriptID: Cloning the 'yay' GitHub repository ..."
  cd ~/Downloads/
  git clone https://aur.archlinux.org/yay.git

  # 2.3 - Build <install> the 'yay' application ...
  echo -en '\n' && sleep 1
  echo " -> $_tagScriptID: Installing <building> the 'yay' GitHub repo ..."
  cd yay/
  makepkg -si

  echo sleep 1
  #! unset sudo   #! <- IMPORTANT !!! ... <- NOT NEEDED ANYMORE !!!
  cd "$_actSrcDir"   #? <- Change directory back into the "Active Working Directory" !!!
fi

# Print a blank line & wait for 3 seconds ...
echo -en '\n' && sleep 3




