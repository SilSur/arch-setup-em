#!/bin/bash


# --- --- --- --- --- --- --- --- --- --- --- --- #
# Erich MALETZKY's best Arch Linux Setup ...      #
# Created: 06-Jun-24                              #
# Updated: 06-Jun-24                              #
# --- --- --- --- --- --- --- --- --- --- --- --- #


echo -en '\n\n' && sleep 1
bsArchEM=ArchEM
echo " ==> Executing the ErichM Arch Linux 'Cool System' Setup <bash> Script ( ID: '$bsArchEM' ) !!! "
echo -en '\n' && sleep 3 


# STEP 1 - Update Arch to lastest instance ...
echo " -> $bsArchEM: STEP 01 - Update Arch to latest version ..."
sudo pacman -Syu --noconfirm

# Print a blank line & wait for 3 seconds ...
echo -en '\n' && sleep 3



# STEP 2 - Install the best AUR Helper -> 'yay' !!!
echo " -> $bsArchEM: STEP 02 - Install the AUR Helper -> 'yay' ..."
pkgS02=yay
if pacman -Qi $pkgS02 > /dev/null ; then
  echo " -> $bsArchEM: The package '$pkgS02' is already installed !!!"
else
  # 2.1 - Install the basic <required> 3rd Party Packages ...
  echo -en '\n' && sleep 1
  echo " -> $bsArchEM: Installing 'base-devel', 'git' & 'wget' ..."
  sudo pacman -S --noconfirm --needed base-devel git wget

  # 2.2 - Clone the 'yay' GitHub Repository ...
  echo -en '\n' && sleep 1
  echo " -> $bsArchEM: Cloning the 'yay' GitHub repo ..."
  cd Downloads/
  git clone https://aur.archlinux.org/yay.git

  # 2.3 - Build <install> the 'yay' application ...
  echo -en '\n' && sleep 1
  echo " -> $bsArchEM: Installing <building> the 'yay' GitHub repo ..."
  cd yay/
  makepkg -si
  cd ~
fi

# Print a blank line & wait for 3 seconds ...
echo -en '\n' && sleep 3



# STEP 3 - Install the Fish (modern) Interactive Shell !!!
echo " -> $bsArchEM: STEP 03 - Install the 'fish' interactive shell from the 'Extras' Arch repo ..."
pkgS03=fish
if pacman -Qi $pkgS03 > /dev/null ; then
  echo " -> $bsArchEM: The package '$pkgS03' is already installed !!!"
else
  # 3.1 - Install fish ...
  echo -en '\n' && sleep 1
  echo " -> $bsArchEM: Installing '$pkgS03' ..."
  yay -S --noconfirm --needed $pkgS03

  # 3.2 - Add <interactive> launch code to 'bashrc' configuration file ...
  echo -en '\n' && sleep 1
  echo " -> $bsArchEM: Configured 'bashrc' to run '$pkgS03' interactively ..."
  echo "" >> ~/.bashrc
  echo "" >> ~/.bashrc
  echo "" >> ~/.bashrc
  echo "# Launch '$pkgS03' upon every new bash instance ..." >> ~/.bashrc
  echo "exec $pkgS03" >> ~/.bashrc

  #! # 3.3 - Launch fish (in interactive mode) !!!
  #! echo -en '\n' && sleep 1
  #! echo ' -> Launched "fish" in interactive mode ...'
  #! fish      # <- launch fish
  #! sleep 3   # Wait 3 seconds before executing subsequent code ...

  #! # 3.4 - Disable fish greeter (upon new fish instance/launch) ...
  #! echo -en '\n' && sleep 1
  #! echo ' -> Disbaled the "$pkgS03 greeter" !!!'
  #! set -U fish_greeting

  #! # 3.4 - Set the 'PS' character accordingly ...
  #! echo -en '\n' && sleep 1
  #! echo ' -> Changing the "PS" charcter to "fish" interactively ...'
fi

# Print a blank line & wait for 3 seconds ...
echo -en '\n' && sleep 3



# STEP 4 - Install the main Internet Browsers !!!
echo " -> $bsArchEM: STEP 04: Install the main Internet Browsers [ chromium firefox google-chrome opera ] ..."
pkgS04=opera
if pacman -Qi $pkgS04 > /dev/null ; then
  echo " -> $bsArchEM: The package '$pkgS04' is already installed !!!"
else
  # 3.1 - Install Internet Browsers ...
  echo -en '\n' && sleep 1
  echo " -> $bsArchEM: Installing the Internet Browsers [ chromium firefox google-chrome opera ] ..."
  yay -S --noconfirm --needed chromium firefox google-chrome opera
fi

# Print a blank line & wait for 3 seconds ...
echo -en '\n' && sleep 3




