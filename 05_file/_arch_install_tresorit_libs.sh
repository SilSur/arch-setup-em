#!/bin/bash

# Localize <scope & context> important <parent> Script Variables here ...
_tagScriptID="$TAG_ID_SCRIPT"
_srcDir="${HOME}/Downloads/tresorit"
_actWorkDir="$ACT_DIR_ARCH_SETUP_SCRIPT_ErichM"

#? INFO: Install the CRITICAL <required> Arch Linux 3rd Party Libraries <software packages> !!!

# STEP 1 - Install the best Cloud Storage Client -> 'Tresorit' !!!
echo " -> $_tagScriptID: STEP 01 - Install the TRESORIT Client -> < IMPORTANT > !!!"
_pkgID=tresorit
if test -f "$HOME/.local/share/${_pkgID}/${_pkgID}"; then
  echo " -> $_tagScriptID: The package '$_pkgID' is already installed !!!"
else

  # 2.1 - Install the basic <required> 3rd Party Packages ...
  echo -en '\n' && sleep 1
  echo " -> $_tagScriptID: Installing 'base-devel', 'git' & 'wget' ..."
  sudo -E pacman -S --noconfirm --needed base-devel git lxcfs sshfs wget
  #! unset sudo   #! <- IMPORTANT !!! ... <- NOT NEEDED ANYMORE !!!

  # 2.2 - Clone the 'yay' GitHub Repository ...
  echo -en '\n' && sleep 1
  echo " -> $_tagScriptID: Cloning the '$_pkgID' GitHub repository ..."
  mkdir -p $_srcDir
  cd $_srcDir
  wget -c "https://installer.tresorit.com/tresorit_installer.run"

  # 2.3 - Build <install> the 'yay' application ...
  echo -en '\n' && sleep 1
  echo " -> $_tagScriptID: Installing <building> the '$_pkgID' GitHub repo ..."
  sh $_srcDir/tresorit_installer.run

  echo sleep 1
  #! unset sudo   #! <- IMPORTANT !!! ... <- NOT NEEDED ANYMORE !!!
  cd "$_actWorkDir"   #? <- Change directory back into the "Active Working Directory" !!!
fi

# Print a blank line & wait for 3 seconds ...
echo -en '\n' && sleep 3




