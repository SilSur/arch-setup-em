#!/bin/bash

# --- --- --- --- --- --- --- --- --- --- --- #
# Erich MALETZKY's best Arch Linux Setup ...  #
# Created: 06-Jun-24                          #
# Updated: 07-Jun-24                          #
# --- --- --- --- --- --- --- --- --- --- --- #

TAG_ID_SCRIPT="sArchEM"
_tagScriptHeader="
==================================================================================================
|   => Executing the ErichM Arch Linux 'Cool System' Setup <bash> Script ( ID: '$TAG_ID_SCRIPT' ) !!!   |
================================================================================================== "

echo "$_tagScriptHeader"
echo -en '\n' && sleep 3

(   # <- 'Sub' shell ... OPENED !!!
    #! NB: All <ephemeral> variables created in this 'sub-shell' lives & dies within the scope of the 'sub-shell' !!! 


    #? Export the Script ID Tag for use in sourced scripts within this sub-shell !!!
    export $TAG_ID_SCRIPT
    #! export ACT_DIR_ARCH_SETUP_SCRIPT_ErichM="$(pwd)/"  # <- Only works if you are actively in the directory.
    #? Define the full path where the 'ArchSetupEM' folder is located on the <current> local directory ...
    export ACT_DIR_ARCH_SETUP_SCRIPT_ErichM="${HOME}/Apps/Setup/ArchSetupEM"
    _actSrcDir="$ACT_DIR_ARCH_SETUP_SCRIPT_ErichM"


    #? STEP 1 - Update Arch to lastest instance ...
    echo " -> $TAG_ID_SCRIPT: STEP 01 - Update Arch to latest version ..."
    if test -f "/var/lib/pacman/db.lck"; then
        sudo rm "/var/lib/pacman/db.lck"   # <- Delete any previously created <broken> PacMan Lock Files !!!
        echo " -> $TAG_ID_SCRIPT: The orphaned PacMan Lock FILE FOUND & DELETED !!!"
    else
        echo " -> $TAG_ID_SCRIPT: NO orphaned PacMan Lock FILE FOUND - proceed as normal ..."
    fi
    sudo -E pacman -Syu --noconfirm
    #! unset sodu  # <- Revert the 'sudo' previliges (apparently not "good practice" for bash scripts)...
    echo -en '\n' && sleep 3

    #? STEP 2 - Install 3rd Party Sotfware as required ... 
    # Activate <uncomment> scripts that are require to install relevant software ...

    #? SET 0 - BASIC ( & CRITICAL ) Software ...
    #! Setup 01 - Critical <required> software <<< MUST ALWAYS BE STEP 1 & SHOULD NEVER BE SKIPPED !!! >>> ...
    source ${_actSrcDir}/00_base/_arch_install_critical_libs.sh   #? <- Installs: 'YAY' ( only - for now ... ) !!!
    source ${_actSrcDir}/00_base/_arch_install_basic_libs.sh
    source ${_actSrcDir}/00_base/_arch_install_worker_libs.sh


    #? SET 1 - INTERNET ( & CHAT ) Software ...
    source ${_actSrcDir}/01_www/_arch_install_browser_libs.sh
    source ${_actSrcDir}/01_www/_arch_install_downloader_libs.sh
    source ${_actSrcDir}/01_www/_arch_install_social_libs.sh


    #? SET 2 - THEMES ( & FONTS ) Software ...
    source ${_actSrcDir}/02_theme/_arch_install_font_libs.sh
    source ${_actSrcDir}/02_theme/_arch_install_theme_libs.sh


    #? SET 3 - MEDIA ( AUDIO & VIDEO ) Software ...
    source ${_actSrcDir}/03_media/_arch_install_audio_libs.sh
    source ${_actSrcDir}/03_media/_arch_install_video_libs.sh


    #? SET 4 - OFFICE ( SUITE & RefMan ) Software ...
    source ${_actSrcDir}/04_office/_arch_install_office_libs.sh
    source ${_actSrcDir}/04_office/_arch_install_online_libs.sh


    #? SET 5 - FILE ( BROWSER & SYNC ) Software ...
    source ${_actSrcDir}/05_file/_arch_install_file_libs.sh
    source ${_actSrcDir}/05_file/_arch_install_sync_libs.sh
    source ${_actSrcDir}/05_file/_arch_install_tresorit_libs.sh


    #? SET 6 - IMAGE ( STILL MEDIA & GRAPHIC ) Software ...
    source ${_actSrcDir}/06_image/_arch_install_graphic_libs.sh


    #? SET 7 - CODE ( SDK & IDE ) Software ...
    source ${_actSrcDir}/07_code/_arch_install_ide_libs.sh
    source ${_actSrcDir}/07_code/_arch_install_sdk_android_libs.sh
    source ${_actSrcDir}/07_code/_arch_install_sdk_flutter_libs.sh
    source ${_actSrcDir}/07_code/_arch_install_sdk_misc_libs.sh


    #? SET 8 - MISC ( RANDOM & FAVs ) Software ...
    source ${_actSrcDir}/08_misc/_arch_install_favs_libs.sh
    source ${_actSrcDir}/08_misc/_arch_install_test_libs.sh


    #? SET 9 - SHELLs ( the Friendly Interactive SHell ) Software ...
    source ${_actSrcDir}/09_shells/_arch_install_fish_libs.sh


)   # <- 'Sub' shell ... CLOSED !!!

