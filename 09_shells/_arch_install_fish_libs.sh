#!/bin/bash

# Localize <scope & context> the Script ID Tag ...
_tagScriptID="$TAG_ID_SCRIPT"
_fileBashRC="${HOME}/.bashrc"
_typeApps="Friendly Interactive Shell"
_fileFishFUNCs="${HOME}/.config/fish/functions"

#? INFO: Install the $_typeApps <required> Arch Linux 3rd Party Libraries <software packages> !!!

# STEP 1 - Install the Fish (modern) Interactive Shell !!!
echo " -> $_tagScriptID: STEP 01 - Install the Friendly Interactive Shell < FISH > from the 'Extras' Arch repo ..."
pkgFISH=fish
if pacman -Qi $pkgFISH > /dev/null ; then
  echo " -> $_tagScriptID: The package '$pkgFISH' is already installed !!!"
else
  # 1.1 - Install fish ...
  echo -en '\n' && sleep 1
  echo " -> $_tagScriptID: Installing '$pkgFISH' ..."
  yay -S --noconfirm --needed $pkgFISH
fi

# STEP 2 - Add the FISH Execute Command to the .bashrc file (accordingly) ...
if grep -q "exec fish" $_fileBashRC; then
  echo " -> $_tagScriptID: The FISH Execute command already exists in the BashRC File !!!"
else
  echo " -> $_tagScriptID: Inserted the FISH Execute command into '~/.bashrc' !!!"
  echo -en '\n\n\n' >> $_fileBashRC   # <- Create 3 blank rows to denote distinct section in '.bashrc' file.
  echo "# Launch '$pkgFISH' on every new bash instance ..." >> $_fileBashRC
  echo "exec $pkgFISH" >> $_fileBashRC
fi

# STEP 3 - Deactivate the FISH Greeter ( to not display at every new FISH instance ) ...
#! fish   #? <- Launch a new "FISH" instance ...
echo " -> $_tagScriptID: Deactivated the 'FISH' Greeter !!!"
echo -en '\n' && sleep 1
fish -c 'set -U fish_greeting'

# STEP 4 - Configure a few important "FISH" aliases ...
echo " -> $_tagScriptID: Configured 2 'FISH' Aliases [ 'ls' 'lsl' ] !!!"
fish -c 'alias ls "lsd -a"; funcsave ls'
fish -c 'alias ll "lsd -al"; funcsave ll'

# Print a blank line & wait for 1 seconds ...
echo -en '\n' && sleep 3




