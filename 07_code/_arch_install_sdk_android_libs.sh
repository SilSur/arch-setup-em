#!/bin/bash

# Localize <scope & context> the Script ID Tag ...
_tagScriptID="$TAG_ID_SCRIPT"
_typeApps="ANDROID SDK"
_fileBashRC="${HOME}/.bashrc"
_dirAndroidSDK="Apps/DevsFoldr/SDKs/Android"

#? INFO: Install the $_typeApps <required> Arch Linux 3rd Party Libraries <software packages> !!!
#? SOURCE ==> https://medium.com/@rajgadhiya011/how-to-setup-flutter-on-arch-linux-with-android-sdk-a-step-by-step-guide-f40450b55669

# STEP 1 - Create a <character> list of the required 3rd Party libraries ...
_pkgList="android-platform  android-sdk  android-sdk-build-tools  android-sdk-cmdline-tools-latest  android-sdk-platform-tools"

# STEP 2 - Install the best AUR Helper -> 'yay' !!!
echo " -> $_tagScriptID: Install $_typeApps Libs -> [ $_pkgList ] ..."
yay -S --noconfirm --needed $_pkgList
echo -en '\n' && sleep 0.5

# STEP 3 - Copy the ANDROID SDK to the User Home Directory ...
echo " -> $_tagScriptID: Copying Android SDK to $_typeApps Libs -> [ $_pkgList ] ..."
echo '' && sleep 1  # <- Output <echo> a blank string to the console/terminal & wait for 1 second !!!
mkdir -p "$HOME/$_dirAndroidSDK"
rsync -avz "/opt/android-sdk/" "$HOME/$_dirAndroidSDK/"  # <- Copy all Android SDK files to the user {HOME} directory ...
chown -R $USER:$USER "$HOME/$_dirAndroidSDK"  # <- Set the owner of all files to the active user.

# STEP 4 - Add Environmental Variables to the .bashrc file (accordingly) ...
if grep -q "ANDROID_HOME" $_fileBashRC; then
  echo " -> $_tagScriptID: Updated ANDROID & JAVA SDKs Environmental variables in '~/.bashrc' !!!"
else
  echo " -> $_tagScriptID: Inserted ANDROID & JAVA Environmental variables in '~/.bashrc' !!!"
  echo -en '\n\n\n' >> $_fileBashRC   # <- Create 3 blank rows to denote distinct section in '.bashrc' file.
  echo "# The Environment Variables that ensure proper Android & JAVA SDKs functionality ..." >> $_fileBashRC
  echo "export JAVA_HOME=/usr/bin" >> $_fileBashRC
  echo "export ORG_GRADLE_CACHING=true" >> $_fileBashRC
  echo "export ANDROID_HOME=\$HOME/${_dirAndroidSDK}" >> $_fileBashRC
  echo "export ANDROID_SDK_ROOT=\$HOME/${_dirAndroidSDK}" >> $_fileBashRC
  echo "export PATH=\$PATH:\$ANDROID_HOME/tools" >> $_fileBashRC
fi

# STEP 4 - Print a blank line & wait for 3 seconds <post install> ...
echo -en '\n' && sleep 3




