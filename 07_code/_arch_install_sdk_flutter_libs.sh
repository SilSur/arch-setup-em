#!/bin/bash

# Localize <scope & context> the Script ID Tag ...
_tagScriptID="$TAG_ID_SCRIPT"
_typeApps="FLUTTER SDK"
_strFltrVers="3.22.2-stable"
_fileBashRC="${HOME}/.bashrc"
_dirFlutterDL="${HOME}/Downloads/FlutterSDK"
_dirGoogleChrome="/usr/bin/google-chrome-stable"   #! <- Result of: "yay -S google-chrome" !!!
_dirFlutterSDK="${HOME}/Apps/DevsFoldr/SDKs/Flutter"

#? INFO: Install the $_typeApps <required> Arch Linux 3rd Party Libraries <software packages> !!!
#? SOURCE ==> https://medium.com/@rajgadhiya011/how-to-setup-flutter-on-arch-linux-with-android-sdk-a-step-by-step-guide-f40450b55669

# STEP 1 - Create a <character> list of the required 3rd Party libraries ...
_pkgList="bash  cmake  file  mkdir  ncurses  ninja  rm  which"
#! _pkgList="flutter-common  flutter-devel  flutter-gradle  flutter-material-fonts-google-bin  flutter-target-android  flutter-target-linux  flutter-target-web  flutter-tool"

# STEP 2 - Install the best AUR Helper -> 'yay' !!!
echo " -> $_tagScriptID: Install $_typeApps Libs -> [ $_pkgList ] ..."
yay -S --noconfirm --needed $_pkgList

# STEP 3 - Download the Flutter SDK from the official Flutter Dev Website ...
mkdir -p $_dirFlutterDL
mkdir -p $_dirFlutterSDK
cd $_dirFlutterDL
echo " -> $_tagScriptID: Download Flutter SDK v$_strFltrVers from Official Website ..."
wget -c "https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_${_strFltrVers}.tar.xz"
tar -xf "${_dirFlutterDL}/flutter_linux_${_strFltrVers}.tar.xz"
#! tar -xf "${_dirFlutterDL}/flutter_linux_${_strFltrVers}.tar.xz" -C "$_dirFlutterSDK/"
echo -en '\n' && sleep 1

# STEP 4 - Copy the Flutter SDK to the final <local> directory ...
echo " -> $_tagScriptID: Copy Flutter SDK to final <local> directory ( '$_dirFlutterSDK' ) ..."
rsync -avz "$_dirFlutterDL/flutter/" "$_dirFlutterSDK/"  # <- Copy all Flutter SDK files to the user {HOME} directory ...
rm -rf "$_dirFlutterDL/flutter/"

# STEP 5 - Add Environmental Variables to the .bashrc file (accordingly) ...
if grep -q "CHROME_EXECUTABLE" $_fileBashRC; then
  echo " -> $_tagScriptID: Updated Flutter SDK Environmental variables in '~/.bashrc' !!!"
else
  echo " -> $_tagScriptID: Inserted FLUTTER Environmental variables in '~/.bashrc' !!!"
  echo -en '\n\n\n' >> $_fileBashRC   # <- Create 3 blank rows to denote distinct section in '.bashrc' file.
  echo "# The Environment Variables that ensure proper Flutter SDK functionality ..." >> $_fileBashRC
  echo "export CHROME_EXECUTABLE=$_dirGoogleChrome" >> $_fileBashRC
  echo "export PATH=\$PATH:$_dirFlutterSDK/bin" >> $_fileBashRC
fi
export PATH="$PATH:$_dirFlutterSDK/bin"   #? <- Export "PATH" for local shell instance (encapsulated code) !!!
echo -en '\n' && sleep 1

# STEP 6 - FINALLY: accept the Android End User Licenses ...
echo " -> $_tagScriptID: Automatically accept the Android SDK licenses ..."
#! source "$_fileBashRC"   # <- Don't need this bcoz we already exported the "path" on Line 48 above !!!
yes | flutter doctor --android-licenses   # <- Accept Android SDK licenses automatically

# STEP 7 - Print a blank line & wait for 3 seconds <post install> ...
echo -en '\n' && sleep 3




