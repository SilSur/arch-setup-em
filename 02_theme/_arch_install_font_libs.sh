#!/bin/bash

# Localize <scope & context> the Script ID Tag ...
_tagScriptID="$TAG_ID_SCRIPT"
_typeApps="FONT"

#? INFO: Install the $_typeApps <required> Arch Linux 3rd Party Libraries <software packages> !!!

# STEP 1 - Create a <character> list of the required 3rd Party libraries ...
_pkgList="awesome-terminal-fonts"

# STEP 2 - Install the best AUR Helper -> 'yay' !!!
echo " -> $_tagScriptID: Install $_typeApps Libs -> [ nerd-fonts ] ..."
sudo -E pacman -S --noconfirm --needed $(pacman -Sgq nerd-fonts)
echo " -> $_tagScriptID: Install $_typeApps Libs -> [ $_pkgList ] ..."
#! yay -S --noconfirm --needed $_pkgList
yay -S --noconfirm --needed awesome-terminal-fonts

# STEP 3 - Print a blank line & wait for 3 seconds <post install> ...
echo -en '\n' && sleep 3




